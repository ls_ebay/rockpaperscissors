package uk.co.leila.rps;

public class ConsoleGameOutput implements GameOutput{

    public static final String START_MESSAGE = "Enter '1' to play against computer or '2' for computer to play against computer:";
    public static final String THROW_MESSAGE = "Enter 'rock', 'paper' or 'scissors' as your throw:";
    public static final String WINNER_START_MESSAGE = "The winner is: ";
    public static final String PLAYER_V_COMPUTER = "You're player one, the computer is player two and...";
    public static final String COMPUTER_V_COMPUTER = "First computer playing against second computer...";

    private final Console console;

    public ConsoleGameOutput(Console console) {
        this.console = console;
    }

    public void startMessage() {
        console.println(START_MESSAGE);
        }

    public void playerThrowChoices() {
        console.println(THROW_MESSAGE);
    }

    public void gameWinnerIs(GameResult winner) {
        console.println(WINNER_START_MESSAGE + winner.toString());
    }

    public void computerVcomputer() {
        console.println(COMPUTER_V_COMPUTER);
    }

    public void playerVcomputer() { console.println(PLAYER_V_COMPUTER);}
}