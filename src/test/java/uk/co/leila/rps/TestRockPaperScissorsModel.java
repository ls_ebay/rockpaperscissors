package uk.co.leila.rps;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRockPaperScissorsModel {

    RockPaperScissorsModel game = new RockPaperScissorsModel();

    @Test public void paperBeatsRock() {
        assertEquals(game.getWinner(Throw.ROCK, Throw.PAPER), GameResult.PLAYER_TWO);
    }

    @Test public void scissorsBeatsPaper() {
        assertEquals(game.getWinner(Throw.SCISSORS, Throw.PAPER), GameResult.PLAYER_ONE);
    }

    @Test public void rockBeatsScissors() {
        assertEquals(game.getWinner(Throw.SCISSORS, Throw.ROCK), GameResult.PLAYER_TWO);
    }

    @Test public void sameThrowTies() {
        assertEquals(game.getWinner(Throw.ROCK, Throw.ROCK), GameResult.TIE);
        assertEquals(game.getWinner(Throw.PAPER, Throw.PAPER), GameResult.TIE);
        assertEquals(game.getWinner(Throw.SCISSORS, Throw.SCISSORS), GameResult.TIE);
    }
}
