package uk.co.leila.rps;

public interface GameOutput {
    void startMessage();
    void playerThrowChoices();
    void gameWinnerIs(GameResult winner);
    void computerVcomputer();
    void playerVcomputer();
}
