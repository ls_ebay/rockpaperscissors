package uk.co.leila.rps;

public enum GameResult {

    PLAYER_ONE,
    PLAYER_TWO,
    TIE;
}
