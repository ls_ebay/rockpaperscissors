package uk.co.leila.rps;

public class RockPaperScissorsModel implements GameModel {

    public GameResult getWinner(Throw first, Throw second) {
        if (first.equals(second)) {
            return GameResult.TIE;
        } else if ((first.ordinal() - second.ordinal()) % 3 == 1) {
            return GameResult.PLAYER_ONE;
        }
        return GameResult.PLAYER_TWO;
    }
}
