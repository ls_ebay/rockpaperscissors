package uk.co.leila.rps;

public interface Game {

    void startMessage();
    void promptPlayerThrow();
    void computerVcomputer();
    void playerVcomputer(Throw playerThrow);
}
