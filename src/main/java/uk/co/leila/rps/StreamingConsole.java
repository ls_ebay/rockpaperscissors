package uk.co.leila.rps;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class StreamingConsole implements Console {

    private final Scanner scanner;
    private final PrintStream out;

    public StreamingConsole(InputStream in, PrintStream out) {
        scanner = new Scanner(in);
        this.out = out;
    }

    public void println(String content) {
        out.println(content);
    }

    public String readInput() {
        return scanner.nextLine();
    }
}
