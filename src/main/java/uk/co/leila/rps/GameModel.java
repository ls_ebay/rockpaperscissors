package uk.co.leila.rps;

public interface GameModel {
    GameResult getWinner(Throw first, Throw second);
}
