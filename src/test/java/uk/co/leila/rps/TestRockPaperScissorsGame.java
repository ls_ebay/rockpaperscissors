package uk.co.leila.rps;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;
import uk.co.leila.rps.*;

public class TestRockPaperScissorsGame {

    @Rule public JUnitRuleMockery context = new JUnitRuleMockery();

    private final GameOutput output = context.mock(GameOutput.class);
    private final GameModel model = context.mock(GameModel.class);

    private final RockPaperScissorsGame game = new RockPaperScissorsGame(output, model);

    @Test public void promptForPlayerThrowSentToOutput() {
        context.checking(new Expectations() {{
        oneOf(output).playerThrowChoices();}
        });

        game.promptPlayerThrow();
    }

    @Test public void startMessageReqSentToOutput() {
        context.checking(new Expectations() {{
            oneOf(output).startMessage();}
        });
        game.startMessage();
    }

    @Test public void showWinnerWillGetResultFromModelAndSendToOutput() {
        final Throw first = Throw.PAPER;
        final Throw second = Throw.SCISSORS;
        final GameResult result = GameResult.PLAYER_TWO;

        context.checking(new Expectations() {{
            allowing(model).getWinner(first, second);
            will(returnValue(result));
            allowing(output).gameWinnerIs(result);
        }});
        game.showWinner(first, second);
    }
}
