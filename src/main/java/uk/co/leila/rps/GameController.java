package uk.co.leila.rps;

public class GameController {

    private final Game game;

    public GameController(Game game) {
        this.game = game;
    }

    public void handle(String input) {

        if (input.equals("1")) {
            game.promptPlayerThrow();
            return;
        }

        if (input.equals("2")) {
            game.computerVcomputer();
        }

        if (Throw.toValue(input) != null) {
            game.playerVcomputer(Throw.toValue(input));
        }

        game.startMessage();
    }
}
