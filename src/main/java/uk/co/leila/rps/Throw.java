package uk.co.leila.rps;

import java.util.Random;

public enum Throw {
    ROCK,
    PAPER,
    SCISSORS;

    private static final Throw[] VALUES = values();
    private static final int SIZE = VALUES.length;
    private static final Random RANDOM = new Random();

    public static Throw getRandomThrow() {
        return VALUES[RANDOM.nextInt(SIZE)];
    }

    public static Throw toValue(String value) {
        try {
            return Throw.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
