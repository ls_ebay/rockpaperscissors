# Rock-Paper-Scissors exercise for eBay

The application can be run using the following command:

```shell
mvn compile exec:java
```

## How to play:

To start game press 'enter'. Player input choices via the console. 

Player can chose to play against computer, or for computer to play against computer.

## Example game:

```bash
Enter '1' to play against computer or '2' for computer to play against computer:
1
Enter 'rock', 'paper' or 'scissors' as your throw:
paper
You're player one, the computer is player two and...
The winner is: TIE
Enter '1' to play against computer or '2' for computer to play against computer:
2
First computer playing against second computer...
The winner is: PLAYER_TWO
Enter '1' to play against computer or '2' for computer to play against computer:
```
