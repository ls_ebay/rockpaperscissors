package uk.co.leila.rps;

public class GameConsole {

    private Console console;
    private GameController controller;

    public static GameConsole createRPCGameConsole(Console console) {
        ConsoleGameOutput output = new ConsoleGameOutput(console);
        RockPaperScissorsGame game = new RockPaperScissorsGame(output, new RockPaperScissorsModel());
        GameController controller = new GameController(game);
        return new GameConsole(console, controller);
    }

    public GameConsole(Console console, GameController controller) {
        this.console = console;
        this.controller = controller;
    }

    public void start() {

        String input = console.readInput();
        while (true) {
            controller.handle(input);
            input = console.readInput();
        }
    }
}
