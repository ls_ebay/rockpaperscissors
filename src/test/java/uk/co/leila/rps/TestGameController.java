package uk.co.leila.rps;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class TestGameController {

    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    private final Game game = context.mock(Game.class);
    private final GameController controller = new GameController(game);

    @Test public void promptPlayerThrowIfInputIsOne() {
        context.checking(new Expectations() {{
        oneOf(game).promptPlayerThrow();}
        });
        controller.handle("1");
    }

    @Test public void showWinnerIfInputIsTwoAndThenShowStartMessage() {
        context.checking(new Expectations() {{
            allowing(game).computerVcomputer();
            allowing(game).startMessage();
        }});
        controller.handle("2");
    }

    @Test public void showWinnerIfPlayerThrowValidAndThenShowStartMessage() {
        final Throw playerThrow = Throw.getRandomThrow();

        context.checking(new Expectations() {{
        oneOf(game).playerVcomputer(playerThrow);
        oneOf(game).startMessage();}
        });

        controller.handle(playerThrow.toString());
    }

}
