package uk.co.leila.rps;

public class RockPaperScissorsGame implements Game {

    private final GameOutput output;
    private GameModel model;

    public RockPaperScissorsGame(GameOutput output, GameModel model) {
        this.output = output;
        this.model = model;
    }

    public void promptPlayerThrow() {
        output.playerThrowChoices();
    }

    public void startMessage() {
        output.startMessage();
    }

    public void showWinner(Throw first, Throw second) {
        output.gameWinnerIs(model.getWinner(first, second));
    }

    public void computerVcomputer() {
        output.computerVcomputer();
        showWinner(Throw.getRandomThrow(), Throw.getRandomThrow());
    }

    public void playerVcomputer(Throw playerThrow) {
        output.playerVcomputer();
        showWinner(playerThrow, Throw.getRandomThrow());
    }
}
