package uk.co.leila.rps;

public class Main {
    public static void main(String[] args) {
        GameConsole app = GameConsole.createRPCGameConsole(new StreamingConsole(System.in, System.out));
        app.start();
    }
}
