package uk.co.leila.rps;

public interface Console {

    void println(String content);
    String readInput();

}
