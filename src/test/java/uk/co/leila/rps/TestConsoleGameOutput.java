package uk.co.leila.rps;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestConsoleGameOutput {

    private final FakeConsole console = new FakeConsole();
    private final ConsoleGameOutput output = new ConsoleGameOutput(console);

    @Test
    public void startMessagePrintedToConsole() {
        output.startMessage();
        assertThat(console.receivedString(), equalTo(ConsoleGameOutput.START_MESSAGE));
    }

    @Test
    public void playerThrowChoicesPrintedToConsole() {
        output.playerThrowChoices();
        assertThat(console.receivedString(), equalTo(ConsoleGameOutput.THROW_MESSAGE));
    }

    @Test
    public void gameWinnerPrintedToConsole() {
        output.gameWinnerIs(GameResult.PLAYER_ONE);
        assertThat(console.receivedString(), equalTo(ConsoleGameOutput.WINNER_START_MESSAGE + GameResult.PLAYER_ONE.toString()));

    }

    @Test
    public void computerVcomputerMsgPrintedToConsole() {
        output.computerVcomputer();
        assertThat(console.receivedString(), equalTo(ConsoleGameOutput.COMPUTER_V_COMPUTER));
    }

    private class FakeConsole implements Console {

        private String receivedString;

        public void println(String content) {
            receivedString = content;
        }

        public String readInput() {
            return "";
        }

        public String receivedString() {
            return receivedString;
        }
    }
}
